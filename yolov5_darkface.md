# 🔥 基于DARK FACE的YOLOv5人脸检测模型的构建

## 创建人：曾逸夫

### 💡 DARK FACE数据集准备

📌 [官网](https://flyywh.github.io/CVPRW2019LowLight/)下载`DarkFace_Train_2021.zip`

📌 下载后解压得到：`image`和`label`

📌 DARK FACE标注如下：

```
5
395 324 422 350
455 310 477 335
572 315 612 355
672 301 716 344
504 340 508 344
```

📌 DARK FACE数据集原始数据分布

| 数据集名称 |  实例数  | 图片数  | 标记图片数 |
| :---: | :---: | :--: | :---: |
| image | 50399 | 6000 | 6000  |

### 💡 YOLOv5训练集和验证集的制作

#### ⚡ 训练集和验证集分布

❤️ 将原始数据集清洗后重新分布，如下表：

| 数据集名称 |  实例数  | 图片数  | 实例占比（%） | 图片占比（%） |
| :---: | :---: | :--: | :-----: | :-----: |
|  训练集  | 40200 | 4769 |  79.76  |  79.48  |
|  验证集  | 10199 | 1231 |  20.24  |  20.52  |
|  总数   | 50399 | 6000 |   100   |   100   |

### 💡 人脸检测模型的构建

📌 darkface.yaml

```yaml
path: ../datasets/darkface  # dataset root dir
train: images/train  # train images (relative to 'path') 128 images
val: images/val  # val images (relative to 'path') 128 images
test:  # test images (optional)

# Classes
nc: 1  # number of classes
names: ['face']  # class names
```

📌 yolov5m.yaml（部分）

```yaml
# YOLOv5 🚀 by Ultralytics, GPL-3.0 license

# Parameters
nc: 1  # number of classes
depth_multiple: 0.67  # model depth multiple
width_multiple: 0.75  # layer channel multiple
```

📌 hyp.scratch-low.yaml （默认配置）

```yaml
# YOLOv5 🚀 by Ultralytics, GPL-3.0 license
# Hyperparameters for low-augmentation COCO training from scratch
# python train.py --batch 64 --cfg yolov5n6.yaml --weights '' --data coco.yaml --img 640 --epochs 300 --linear
# See tutorials for hyperparameter evolution https://github.com/ultralytics/yolov5#tutorials

lr0: 0.01  # initial learning rate (SGD=1E-2, Adam=1E-3)
lrf: 0.01  # final OneCycleLR learning rate (lr0 * lrf)
momentum: 0.937  # SGD momentum/Adam beta1
weight_decay: 0.0005  # optimizer weight decay 5e-4
warmup_epochs: 3.0  # warmup epochs (fractions ok)
warmup_momentum: 0.8  # warmup initial momentum
warmup_bias_lr: 0.1  # warmup initial bias lr
box: 0.05  # box loss gain
cls: 0.5  # cls loss gain
cls_pw: 1.0  # cls BCELoss positive_weight
obj: 1.0  # obj loss gain (scale with pixels)
obj_pw: 1.0  # obj BCELoss positive_weight
iou_t: 0.20  # IoU training threshold
anchor_t: 4.0  # anchor-multiple threshold
# anchors: 3  # anchors per output layer (0 to ignore)
fl_gamma: 0.0  # focal loss gamma (efficientDet default gamma=1.5)
hsv_h: 0.015  # image HSV-Hue augmentation (fraction)
hsv_s: 0.7  # image HSV-Saturation augmentation (fraction)
hsv_v: 0.4  # image HSV-Value augmentation (fraction)
degrees: 0.0  # image rotation (+/- deg)
translate: 0.1  # image translation (+/- fraction)
scale: 0.5  # image scale (+/- gain)
shear: 0.0  # image shear (+/- deg)
perspective: 0.0  # image perspective (+/- fraction), range 0-0.001
flipud: 0.0  # image flip up-down (probability)
fliplr: 0.5  # image flip left-right (probability)
mosaic: 1.0  # image mosaic (probability)
mixup: 0.0  # image mixup (probability)
copy_paste: 0.0  # segment copy-paste (probability)
```

❤️ 执行训练指令

```shell
python train.py --weights ./yolov5m.pt --cfg ./models/yolov5m.yaml --data ./data/darkface.yaml --epochs 200 --batch-size 32 --name darkface
```

📌 将`best.pt`更名为`darkface-m.pt`

### 💡 训练结果分析和总结

📌 训练参数

|    属性名称    |        属性参数         |
| :--------: | :-----------------: |
|     层数     |         369         |
|   params   |      20871318       |
|   GFLOPs   |        48.2         |
|  GPU 显存占用  |       13.7 G        |
|     用时     |       2.429 h       |
| Batch Size |         32          |
|   epoch    |         200         |
|    hyps    | hyp.scratch-low（默认） |

❗ 注：`其他训练参数`为YOLOv5默认参数

📌 模型训练指标（YOLOv5默认评价标准）

|  类别  |  AP   | AP50  |
| :--: | :---: | :---: |
| face | 0.287 | 0.636 |

📌 W&B训练AP和AP50

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/darkface_m_AP.png">
</div>

📌 W&B训练和验证损失

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/darkface_m_train_val.png ">
</div>

❤️ 模型下载

|    模型名称    |                               下载地址                                |  模型大小  |   适用范围   | 适用设备 |
| :--------: | :---------------------------------------------------------------: | :----: | :------: | :--: |
| darkface-m | [百度云](https://pan.baidu.com/s/1B_Di3eleWZjHFAt3xXd9Og) , 提取码：mm2k | 42.2MB | 实时,图片,视频 | GPU  |
